from django.shortcuts import render, redirect
from .models import TodoList, TodoItem
from .forms import CreateForm, CreateTodoItemForm

# Create your views here.


def list_todolist(request):
    list_todolist_all = TodoList.objects.all()
    context = {"list_todolist_all": list_todolist_all}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list_object = TodoList.objects.get(id=id)
    context = {"todo_list": todo_list_object, "id": id}
    return render(request, "todos/view.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            todo_list = form.save(commit=False)
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = CreateForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    todo_list_obj = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = CreateForm(request.POST, instance=todo_list_obj)
        if form.is_valid():
            todo_list_obj = form.save(commit=False)
            todo_list_obj.save()
            return redirect("todo_list_detail", id=todo_list_obj.id)
    else:
        form = CreateForm(instance=todo_list_obj)
    context = {"form": form}
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    deleting_obj = TodoList.objects.get(id=id)
    if request.method == "POST":
        deleting_obj.delete()
        return redirect("/todos/")
    return render(request, "todos/delete.html")


def create_todo_item(request):
    todo_lists = TodoList.objects.all()
    if request.method == "POST":
        form = CreateTodoItemForm(request.POST)
        if form.is_valid():
            todo_item_obj = form.save(commit=False)
            todo_item_obj.save()
            return redirect("todo_list_detail", id=todo_item_obj.list.id)
    else:
        form = CreateTodoItemForm()
    context = {"form": form, "todo_lists": todo_lists}
    return render(request, "TodoItem/create.html", context)


def update_todo_item(request, id):
    todo_item_obj = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = CreateTodoItemForm(request.POST, instance=todo_item_obj)
        if form.is_valid():
            todo_item_obj = form.save(commit=False)
            todo_item_obj.save()
            return redirect("todo_list_detail", id=todo_item_obj.list.id)
    else:
        form = CreateTodoItemForm(instance=todo_item_obj)
    context = {"form": form, "todo_item_obj": todo_item_obj}
    return render(request, "TodoItem/update.html", context)
