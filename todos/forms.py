from django import forms
from .models import TodoList, TodoItem


class CreateForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class CreateTodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]
