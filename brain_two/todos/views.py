from django.shortcuts import render
from todos.models import TodoList, TodoItem

# Create your views here.


def get_todolist_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "list.html", context)


def todo_list_detail(request, id):
    todo_list_details = TodoList.objects.get(id=id)
    # html de kullandigin objenin adi burada tanimlaniyor.
    # Context icerisinde "key": value pairleri olusturuyorsun.
    # Contexti, template i render ederken parametre olarak veriyosun (3 satir asagida)
    # Template icinde, bu key ile ("todo_list_details") objeye erisebilirsin.
    context = {"todo_list_details": todo_list_details}
    return render(request, "details.html", context)


# context te key value pair ederek tutuyoruz objenin ozelliklerini/
# return de web browser dan gelen request ile html i birlestirip,
# context teki (database den gelen verilerle)
#  birlestirip view function i sayesinde view da ggosteriyoruz.
