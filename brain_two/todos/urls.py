from django.urls import path
from todos.views import get_todolist_list, todo_list_detail

# Burada view i path icin register ettim.
urlpatterns = [
    path("", get_todolist_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
]
