from django.db import models

# Create your models here.


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    # Burada soyle gizli bir property var
    # items = [TodoItem, TodoItem]
    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.TextField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        TodoList,
        # TodoList modeline items isminde bir property olusturur.
        # Bir TodoList objesine .items yazarak o
        # TodoList in TodoItem larina erisebilirsin.
        related_name="items",
        on_delete=models.CASCADE,
    )


""" TodoItem = [ { sunu yap, su tarihe kadar  bitti, bitmedi}
            {otekini yap, su tarihe kadat, bitti},
            {sonra bi de bunu yap}]
 """
